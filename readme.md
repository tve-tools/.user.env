### Basic user setting
```bash
echo '. ~/.user.env/bootstrap' >> ~/.bashrc

```

### User setting
```bash
git clone https://gitlab.com/tve-tools/.user.env.git
cat ~/.user.env/bootstrap |grep tve |grep -v who |grep -v sed |sed -e 's:# ::' -e 's:~:~:' >> ~/.bashrc
echo tve >>~/.bashrc

```


### System wide
```bash
git clone https://gitlab.com/tve-tools/.user.env.git /etc/profile.d/.user.env
ln -s .user.env/bootstrap /etc/profile.d/custom.sh
# comment PS1 export in ~/.bashrc


```

