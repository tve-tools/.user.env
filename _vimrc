
autocmd FileType sql,sh,python setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType python setlocal softtabstop=2
autocmd BufNewFile,BufRead *Readme*,*envfile* set filetype=sh
autocmd BufNewFile,BufRead *cql* set filetype=sql

set dir=$HOME/tmp,C:\\TEMP,C:\\WINNT\\Temp
source $ENV_DIR/exrc

set guioptions-=T

set guioptions+=a
set smartindent

"set shiftwidth=2
"set tapstop=2
"set expandtab


map <F1> :let &hlsearch=!&hlsearch<CR>
"map <F2> :set syntax=log4j2<CR>
map <F2> :source $ENV_DIR/hi-err.vim<CR>
map <F3> :syntax reset<CR>
map <F6> :set syntax=log<CR>
map <F4> i<C-R>=strftime("%c")<CR><Esc>
nnoremap <F5> "=strftime("%d/%m/%y %H:%M:%S")<CR>P
inoremap <F5> <C-R>=strftime("%d/%m/%y %H:%M:%S")<CR>
map <C-a> :%s :&lt;:<:ge<CR>:%s :&gt;:>:ge<CR>

syntax on

" CTRL-C and CTRL-Insert are Copy
vnoremap <C-C> "+y
vnoremap <C-Insert> "+y
" check c:/apps/vim/vim70/mswin.vim

" go to last position in previous file open: `"
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

" To remove 'highlight search' option within vim context
set nohlsearch

:filetype on

set guifont=Lucida\ Console:h8
if has("gui_running")
  set background=dark
  colorscheme desert
endif


"source $ENV_DIR/comment.vim
autocmd FileType ant,xml,xsd map q :s :^\(\s*\)<:\1<!--: :s :>\(\s*\)$:-->\1:j
autocmd FileType ant,xml,xsd map Q :s :<!--:<: :s :-->\(\s*\)$:>\1:j
autocmd FileType java,jsp,scala,sbt map q :s :^://:j
autocmd FileType java,jsp,scala,sbt map Q :s :^//::j
autocmd FileType vi,vim map q :s :^:":j
autocmd FileType vi,vim map Q :s :^"::j
autocmd FileType sql map q :s :^:--:j
autocmd FileType sql map Q :s :^--::j
autocmd FileType dosbatch,registry map q :s :^:REM :j
autocmd FileType dosbatch,registry map Q :s :^REM ::j
autocmd FileType xdefaults map q :s :^:!:j
autocmd FileType xdefaults map Q :s :^!::j

autocmd FileType css map q :s :^:/*: :s :$:*/:<CR>j
autocmd FileType css map Q :s :^/\*:: :s :\*/$::<CR>j




"source $ENV_DIR/highlightgroups.vim
" http://vimdoc.sourceforge.net/htmldoc/pattern.html#:match
"highlight y cterm=underline ctermbg=yellow gui=underline guibg=yellow
highlight y ctermbg=yellow guibg=yellow
highlight g ctermbg=lightgreen guibg=lightgreen
highlight b ctermbg=lightblue guibg=lightblue
highlight r ctermbg=red guibg=red
" usage: use the above with in command mode during vim edit as follows:
" :match y /iscontainer.*/
" :2match g /iscontainer.*/

" autosave _notes.txt
function! FocusLost_SaveFiles()
  exe ":au FocusLost" expand("%") ":wa"
endfunction

nnoremap <F6> :call FocusLost_SaveFiles()<CR>

"autocmd VimEnter * if @% == 'autosave.vim' | call FocusLost_SaveFiles() | else | echo 'world' | endif
autocmd VimEnter * if @% == '_notes.txt' | call FocusLost_SaveFiles() | endif

