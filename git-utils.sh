#/bin/bash

script_name=$(basename $0)

upload-repos() {
  for file in $(ls); do
    [ "x$file" == "x$script_name" ] && continue
    cd $file
    (! git remote -v |grep 'origin' 2>&1 >/dev/null) && echo 'MISSING origin remote repos !!!' && git remote -v && echo -e '\n' && cd - >/dev/null && continue
    (! git remote -v |grep 'misc' 2>&1 >/dev/null) && echo 'MISSING misc remote repos !!!' && git remote -v && echo -e '\n' && cd - >/dev/null && continue
    ( git remote -v |grep origin 2>&1 >/dev/null) && git pull origin
    ( git remote -v |grep misc 2>&1 >/dev/null) && git push misc
    cd - >/dev/null
  done

}

create-repos() {
  echo '
set-proxy
create-glab-repo myfiles.wiki 14092023
git remote add misc https://gitlab.com/tve-pro/sg/repos/myfiles.wiki.git
git push misc

  '

}

#upload-repos
"$@" || ( echo 'available options: ' && declare -F | sed -e 's:declare -f::' )
[[ $# == 0 ]] && ( echo 'available options: ' && declare -F | sed -e 's:declare -f::' )


