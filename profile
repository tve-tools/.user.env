#!/bin/bash

which timedatectl >/dev/null 2>&1 && timedatectl >/dev/null 2>&1 && (! timedatectl | grep Paris >/dev/null 2>&1) && sudo timedatectl set-timezone Europe/Paris

case "$TERM" in
xterm*|cygwin)


# to get color code, run the following:
# for c in {0..255}; do tput setaf $c; tput setaf $c | cat -v; echo =$c; done
RESET="\[$(tput sgr0)\]"
GREEN="\[$(tput setaf 2)\]"
BLUE="\[$(tput setaf 33)\]"
YELLOW="\[$(tput setaf 3)\]"
UNDERLINE="\[$(tput smul)\]"

bold=$(tput bold)
black=$(tput setaf 0)
red=$(tput setaf 1)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)
white=$(tput setaf 7)
hi=$(tput smso)
REVERSE=$(tput rev)

#WSLENV=""
[ $WSL_DISTRO_NAME ] && WSLENV="$YELLOW${REVERSE}[WSL]$RESET "

# cf.: https://linuxcommand.org/lc3_adv_tput.php
# warning: https://forums.linuxmint.com/viewtopic.php?t=50930

    [ -f /usr/share/git/completion/git-prompt.sh ] && source /usr/share/git/completion/git-prompt.sh
#    typeset -f __git_ps1

#		GREEN="\[\e[0;32m\]"
#		BLUE="\[\e[0;34m\]"
#		BOLDBLUE="\[\e[01;34m\]"
#		BOLDGREEN="\[\e[01;32m\]"
#		UNDBLUE="\[\e[4;34m\]" # underlined blue
#		NOCOLOUR="\[\e[0m\]"
#    [ "x$BKG" == "x" ] && BLUE=$BOLDBLUE
		PS1="\A $BLUE"'$(__git_ps1 "(%s) ")'"$WSLENV$UNDERLINE$GREEN\u@\h$RESET:\w \$ "
#		PS1="\A $UNDERLINE$GREEN\u@\h$RESET:\w \$ "'$(__git_ps1)'
#		PS1=`date +%s`"\A $UNDERLINE$GREEN\u@\h$RESET:\w \$ "
#		PS1="\A $UNDERLINE$GREEN\u@\h$RESET:\w $(__git_ps1) \$ "
#		PS1="\A \u@$BLUE\h$NOCOLOUR:\w \[$bold\]\[$yellow\]TOTO\[$reset\] \$ "

		# the below sets the windows title
    PS1="\[\e]0;\u@\h: "'$(pwd -P)'"\a\]$PS1"


		[ -f $ENV_DIR/dir_colors ] && eval `dircolors -b $ENV_DIR/dir_colors`
	;;
*)
	export PS1=`date +%H:%M`\ `whoami|awk '{print $1}'`@`hostname`:'$PWD'\ \$\ 
	;;
esac

[ "x$BKG" != "x" ] && XMODIFIERS=$BKG

unset MAILCHECK

export CDPATH=.:$HOME
export PATH=$PATH:.:/usr/sbin:/usr/ccs/bin:/usr/local/bin
export PATH=~/bin:$PATH
export MANPATH=$MANPATH:/usr/man:/usr/openwin/share/man

export ENVDIR=$ENV_DIR

case `uname` in
CYGWIN*)
	if [ "$ANT_HOME" != "" ]
	then
		export ANT_HOME=$(cygpath $ANT_HOME)
		export PATH=$PATH:$ANT_HOME/bin
	fi
	if [ "$JAVA_HOME" != "" ]
	then
		export JAVA_HOME=$(cygpath $JAVA_HOME)
		export CLASSPATH=$JAVA_HOME/jre/lib/rt.jar:.
		export PATH=$JAVA_HOME/bin:$PATH
	fi

	export CDPATH=$CDPATH:/cygdrive/c
#	shopt -s histappend
#	export HISTFILE=$HOME/.sh_history

	ID=$(id |grep root)
	if [[ ! -z "$ID" ]]
	then
		echo -ne '\e]11;#FFCCCB\a'
	fi
	;;
*)
	;;
esac

export SQLPATH=$ENV_DIR
case `uname` in
CYGWIN*)
	[ $(id |grep "Niveau obligatoire élevé" |wc -l) == 1 ] && echo -ne '\e]11;#FFBBBB\a'
	SQLPATH=$(cygpath -w -p $SQLPATH)
	;;
*)
	;;
esac

export EDITOR=vi

export ENV=${ENV:=$ENV_DIR/envfile}

#HISTTIMEFORMAT="%d/%m/%y %T "
#HISTSIZE=40000
#HISTFILESIZE=100000
#HISTCONTROL=erasedups:ignorespace
#export PROMPT_COMMAND="history -a; history -r"

# moved to envfile.lite
#[ ! -d ~/.history.d ] && mkdir ~/.history.d
##HISTCONTROL=ignoredups:erasedups
#HISTCONTROL=ignoredups
#HISTSIZE=1000
#HISTFILESIZE=100000
#HISTTIMEFORMAT="%F %T "
#HISTFILE=~/.history.d/bash-$(who -m |cut -d' '  -f 1)-hist
#shopt -s histappend                      # append to history, don't overwrite it
## Save and reload the history after each command finishes
#export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

export MSYS=winsymlinks

if hostname -s >/dev/null 2>&1; then _HOST=$(hostname -s); else _HOST=$(hostname); fi
_ENVFILE=$ENV_DIR/profile.$_HOST
[ -f $_ENVFILE ] && . $_ENVFILE
