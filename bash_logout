#!/bin/bash

UBlue='\033[4;34m'
Blue='\033[0;34m'
NC='\033[0m' # No Color

[ "$(ps -fe |grep mintty |wc -l)" -le "2" ] && echo -e "${UBlue}Ne pas oublier d'executer:${Blue} "'$ENV_DIR'"/bash_logout.$(hostname)${NC} "

_RCFILE=$ENV_DIR/bash_logout.$(hostname)
[ "$(ps -fe |grep mintty |wc -l)" -le "2" ] && read -t 1
[ $? == 0 ] && [ -r $_RCFILE ] && . $_RCFILE

